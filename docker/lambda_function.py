import sys
import logging
import traceback
import os
import json
from datetime import datetime,timedelta
from zoneinfo import ZoneInfo
from utils import scrape_site, load_config, extract_info, save_info, create_feed_xml, query_db
from utils_devtool import search_elem_place
from utils_lambda import download_from_s3, upload_to_s3, upload_directory_to_s3
import psutil # type: ignore


sys.dont_write_bytecode = True

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def handler(event, context):
    try:
        os.environ['HOME'] = '/tmp'

        if len(event.get('TARGET_URL_FOR_DEBUG',"")) > 0:
            logger.setLevel(logging.DEBUG)
            logger.info("Debug started")
            target_entry_xpath = event.get('TARGET_ENTRY_XPATH_FOR_DEBUG') or None
            target_elem_xpath = event.get('TARGET_ELEM_XPATH_FOR_DEBUG') or None
            search_elem_place(event['TARGET_URL_FOR_DEBUG'],entries_xpath=target_entry_xpath,target_elem_xpath=target_elem_xpath)
            # scrape_site(event['TARGET_URL_FOR_DEBUG'])
            storage_used = psutil.disk_usage('/tmp').used / (1024 * 1024)  # MB
            logger.info(f"Ephemeral Storage Used: {storage_used:.2f} MB")
            return {
                'statusCode': 200,
                'body': "Debug finished"
            }

        logger.info("Initializing")

        # db_path = "./info.db"
        db_path = "/tmp/info.db"  # データベースファイルを/tmpディレクトリに配置
        
        s3_sqlite_bucket = os.getenv('S3_SQLITE_BUCKET')
        s3_sqlite_object = os.getenv('S3_SQLITE_OBJECT')
        s3_feed_upload_bucket = os.getenv('S3_FEED_UPLOAD_BUCKET')
        s3_feed_upload_base_path = os.getenv('S3_FEED_UPLOAD_BASE_PATH')
        rss_sites_bucket = os.getenv('RSS_SITES_BUCKET')
        rss_sites_object = os.getenv('RSS_SITES_OBJECT')


        remote_mode = not ('AWS_LAMBDA_FUNCTION_NAME' in os.environ)

        if not remote_mode:  # Lambdaで実行されている場合
            # SQLiteファイルのダウンロード
            download_success = download_from_s3(s3_sqlite_bucket, s3_sqlite_object, db_path)
            if download_success==True:
                logger.info("SQLite file downloaded and ready for processing")
            else:
                logger.info("SQLite file does not exist, proceeding without it")

            # rss_sites.jsonのダウンロード
            rss_sites_path = "/tmp/rss_sites.json"
            download_success = download_from_s3(rss_sites_bucket, rss_sites_object, rss_sites_path)
            if download_success==True:
                logger.info("rss_sites.json downloaded and ready for processing")
            else:
                logger.info("rss_sites.json does not exist, proceeding without it")
        else:
            logger.info("Running in local mode")

        config_file_path = "/tmp/rss_sites.json" \
            if os.path.isfile("/tmp/rss_sites.json") and not len(event.get('TARGET_CONFIG_FILE_PATH',"")) > 0 \
            else event.get('TARGET_CONFIG_FILE_PATH','./rss_sites_wikipedia_onthisday.json')
        config = load_config(config_file_path)
        logger.info(f'Config loaded: "{config_file_path}"')

        for site_config in config['sites']:
            # 更新対象かどうか判定
            feed_name = site_config.get('feed_name')
            logger.info(f'Determining if "{feed_name}" is eligible for updating...')
            td = timedelta(hours=int(site_config['feed'].get('scrape_minimum_interval_hours') or 18))
            now = datetime.now(ZoneInfo("Asia/Tokyo"))
            # q = f"select json_extract(params, '$.updated') as 'updated' from feeds_info where feed_name = '{feed_name}'"
            last_actioned_for_feed = query_db(
                f"""
                select 
                    json_extract(params, '$.last_scraped') as 'last_scraped',
                    json_extract(params, '$.updated') as 'updated' 
                from feeds_info 
                where feed_name = '{feed_name}' 
                """
                , db_path
                )[0]
            # q = f"select json_extract(params, '$.last_scraped') as 'last_scraped',json_extract(params, '$.updated') as 'updated' from feeds_info where feed_name = '{feed_name}'"
            # last_scraped = query_db(q, db_path)[0]['last_scraped'] 
            last_scraped = last_actioned_for_feed['last_scraped'] or last_actioned_for_feed['updated']
            estimated_time_of_next_scraping = datetime.fromisoformat(last_scraped) + td if not last_scraped is None else datetime(2000,1,1,tzinfo=ZoneInfo("Asia/Tokyo"))
            if estimated_time_of_next_scraping is None or now < estimated_time_of_next_scraping:
                logger.info(f"The next scraping is due after {estimated_time_of_next_scraping}. Now, {now}.")
                continue
            logger.info(f'"{feed_name}" is eligible for updating. Now, extracting...')

            # 更新対象のようなので、本処理を行う
            feed_info, entry_info_list = extract_info(site_config, remote_mode=remote_mode)
            save_info(feed_info, entry_info_list, db_path)

        logger.info("Scraping finished")

        # create_feed_xml(db_path, config, export_dir_path='./feeds/')
        create_feed_xml(db_path, config, export_dir_path='/tmp/feeds/')
        logger.info("Feed file created")

        # feed_file_ls = os.listdir('./feeds/')
        feed_file_ls = os.listdir('/tmp/feeds/')
        logger.info(f"created feed files: {sorted(feed_file_ls)}")

        if not remote_mode==True:  # Lambdaで実行されている場合
            upload_success = upload_to_s3(db_path, s3_sqlite_bucket, s3_sqlite_object)
            if not upload_success==True:
                logger.error("Failed to upload the SQLite file to S3")
            else:
                logger.info("SQLite file uploaded successfully")

            exclude_patterns = ['.ipynb_checkpoints']
            # upload_success = upload_directory_to_s3('./feeds', s3_feed_upload_bucket, s3_feed_upload_base_path, exclude_patterns=exclude_patterns)
            upload_success = upload_directory_to_s3('/tmp/feeds', s3_feed_upload_bucket, s3_feed_upload_base_path, exclude_patterns=exclude_patterns)
            if not upload_success==True:
                logger.error("Failed to upload the feed files to S3")
            else:
                logger.info("Feed files uploaded successfully")

        storage_used = psutil.disk_usage('/tmp').used / (1024 * 1024)  # MB
        logger.info(f"Ephemeral Storage Used: {storage_used:.2f} MB")

        return {
            'statusCode': 200,
            'body': json.dumps(sorted(feed_file_ls))
        }

    except Exception as e:
        logger.error("Error raised: %s \nTrace: %s", e, traceback.format_exc())
        storage_used = psutil.disk_usage('/tmp').used / (1024 * 1024)  # MB
        logger.info(f"Ephemeral Storage Used: {storage_used:.2f} MB")
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error raised. {e} \n {traceback.format_exc()}')
        }
