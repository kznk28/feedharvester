import logging

def setup_logger(name="custom", level=None):
    """ロガーを取得または初期化する。

    異なるコードパターンで同じように使えるユーティリティ関数です。

    一般的な使用パターン:
    1. クラス（__init__あり）の場合:
        class PDFScraper:
            def __init__(self):
                self.logger = get_logger("PDFScraper")

    2. クラス（__init__なし）の場合:
        class DataConverter:
            logger = get_logger("DataConverter")
            # または
            _logger = None
            
            @classmethod
            def logger(cls):
                if cls._logger is None:
                    cls._logger = get_logger("DataConverter")
                return cls._logger

    3. 関数の場合:
        logger = get_logger("my_functions")
        
        def process_data():
            logger.debug("Processing...")

    Args:
        name: ロガーの名前空間
        level: 明示的にログレベルを指定する場合

    Returns:
        logging.Logger: 設定済みのロガーインスタンス
    """
    logger = logging.getLogger(name)
    
    # まだハンドラが設定されていない場合のみ初期化
    if not logger.handlers:
        logger.propagate = False
        handler = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        
        # デフォルトのログレベル設定
        default_level = level if level is not None else logging.INFO
        logger.setLevel(default_level)
    elif level is not None:
        # 既存のロガーでレベルの指定があった場合は更新
        logger.setLevel(level)
    
    return logger
