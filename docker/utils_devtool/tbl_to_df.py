import pandas as pd # type: ignore
from IPython.display import display # type: ignore
import sqlite3


def tbl_to_df(db_path,q):
    connect = sqlite3.connect(db_path)
    connect.row_factory = sqlite3.Row
    # cursor = connect.cursor()
    df = pd.read_sql_query(q, connect)
    connect.close()

    return df