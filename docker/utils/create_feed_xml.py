import json
import os
import xml.etree.ElementTree as ET
import sqlite3
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def create_feed_xml(db_path,loading_config,export_dir_path='./'):

    if not os.path.exists(export_dir_path):
        os.makedirs(export_dir_path)

    connect = sqlite3.connect(db_path)
    connect.row_factory = sqlite3.Row
    cursor = connect.cursor()
    rows_feeds = cursor.execute("select * from feeds_info").fetchall()
    
    n_feed = 0
    for row_feed in rows_feeds:
    # row_feed = rows_feeds[0]
        n_feed += 1
        if n_feed > loading_config.get('limit_feeds_num',50):
            break
    
        feed_elems = ET.Element("feed", xmlns="http://www.w3.org/2005/Atom")
        feed_name = row_feed['feed_name']
        filtered_feed = list(filter(lambda item: item.get('feed_name') == feed_name, loading_config['sites']))
        if not len(filtered_feed) > 0:
            # DBにフィードは存在するが、config側にそのフィードに該当する情報が無い場合。デバッグ中はままある
            break

        site_config = filtered_feed[0]
        feed_id = row_feed['feed_id']
        
        feed_elems_id = ET.SubElement(feed_elems, "id")
        feed_elems_id.text = feed_id
        
        for key,value in json.loads(row_feed['params']).items():
            # print(key,value)
            if key == 'link':
                feed_elems_link = ET.SubElement(feed_elems, "link", href=value, rel="alternate")
            elif key == 'author':
                feed_elems_author = ET.SubElement(feed_elems, "author")
                feed_elems_author_name = ET.SubElement(feed_elems_author, "name")
                feed_elems_author_name.text = value
            elif key is not None and value is not None:
                exec('feed_elems_' + key + ' = ' + 'ET.SubElement(feed_elems, "' + key + '")') # feed_key = ET.SubElement(feed_elem, key)
                exec('feed_elems_' + key + '.text = value') # feed_key.text = value
            else:
                continue
                
        rows_entries = cursor.execute(
            "select * from entries_info where feed_id = ? order by json_extract(params, '$.updated') desc",(feed_id,)
            ).fetchall()
                
        n_entry=0
        for row_entry in rows_entries:
        # row_entry = rows_entries[0]
        # デバッグ用
            # pprint.pprint(row_entry) 
    
            n_entry += 1
            if n_entry > site_config.get('limit_entries_num',30):
                break
            
            entry_elem = ET.SubElement(feed_elems, "entry")
            
            feed_id = row_entry['feed_id']
            entry_id = row_entry['entry_id']
            entry_id_elem = ET.SubElement(entry_elem, "id")
            entry_id_elem.text = "https://example.com/?entry_id=" + entry_id
        
            # print(f"feed_id: {feed_id}, entry_id: {entry_id}")
            for key,value in json.loads(row_entry['params']).items():
                # print(key,value)
        
                if key == 'link':
                    entry_link = ET.SubElement(entry_elem, "link", href=value)
                elif key is not None and value is not None:
                    exec('entry_' + key + ' = ' + 'ET.SubElement(entry_elem, "' + key + '")') # entry_key = ET.SubElement(entry_elem, key)
                    exec('entry_' + key + '.text = value') # entry_key.text = value
                else:
                    continue
            
        tree = ET.ElementTree(feed_elems)
        tree.write(f'{export_dir_path}{site_config["feed_name"]}_feed.atom', encoding="utf-8", xml_declaration=True)
    
        # デバッグ用
        # ET.indent(tree)
        # ET.dump(tree)
    
    connect.close()


