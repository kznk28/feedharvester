import hashlib


def generate_id(date, title, url,digits=None):
    unique_string = f"{date}-{title}-{url}"
    hash_object = hashlib.sha1(unique_string.encode())
    if digits:
        return hash_object.hexdigest()[:digits]
    else:
        return hash_object.hexdigest()

