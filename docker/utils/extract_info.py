from lxml import html # type: ignore
from urllib.parse import urljoin,urlparse
from zoneinfo import ZoneInfo
from datetime import datetime
import logging

# from utils import scrape_site
from utils.scrape_site import scrape_site
# from utils import generate_id
from utils.generate_id import generate_id



logger = logging.getLogger()
logger.setLevel(logging.INFO)


def extract_info(site_config,limit_entries=100,**kwargs):
    headless_mode = kwargs.get('headless_mode',True) # デフォルトでヘッドレスモード
    remote_mode = kwargs.get('remote_mode',False) # デフォルトでローカルモード（非リモートモード）
    
    soup = scrape_site(site_config['url'],remote_mode=remote_mode,headless_mode=headless_mode)
    lxml_converted_data = html.fromstring(str(soup))
    
    entry_config = site_config['entry']
    
    n=0
    latest_date = None
    entry_info_list = []
    now = datetime.now(ZoneInfo("Asia/Tokyo"))

    
    for elem in site_config['feed']:
        if "_xpath" in elem and len(site_config['feed'][elem]) < 1:
            site_config['feed'][elem] = "/"
    for elem in site_config['entry']:
        if "_xpath" in elem and len(site_config['entry'][elem]) < 1:
            site_config['entry'][elem] = "/"


    entries = lxml_converted_data.xpath(site_config['entries_xpath'])    
    for l in entries:
        # entry_title = ''.join([str(s.replace('　',' ').strip()) for s in l.xpath(entry_config['title_xpath'])])
        entry_title = ''.join([str(s.replace('　',' ')) for s in l.xpath(entry_config['title_xpath'])])
        logger.debug(f"entry_title: {entry_title}")
        # entry_href = urljoin(site_config['url'],''.join([str(s.replace('　',' ').strip()) for s in l.xpath(entry_config['href_xpath'])]))
        entry_href = urljoin(site_config['url'],''.join([str(s.replace('　',' ')) for s in l.xpath(entry_config['href_xpath'])]))
        logger.debug(f"entry_href: {entry_href}")
        # date_obj = ''.join([str(s.replace('　',' ').strip()) for s in l.xpath(entry_config['date_xpath'])])
        date_obj = ''.join([str(s.replace('　',' ')) for s in l.xpath(entry_config['date_xpath'])])
        logger.debug(f"date_obj: {date_obj}")
        if len(date_obj) > 0:
            entry_posted_datetime = datetime.strptime(date_obj.strip(),entry_config['date_format']).replace(tzinfo=ZoneInfo("Asia/Tokyo"))
            logger.debug(f"entry_posted_datetime: {entry_posted_datetime}")
            if entry_posted_datetime.year == 1900:
                entry_posted_datetime = entry_posted_datetime.replace(year=now.year)
                if (entry_posted_datetime - now).days > 10:
                    entry_posted_datetime = entry_posted_datetime.replace(year=(now.year - 1))
            if latest_date is None or entry_posted_datetime > latest_date:
                latest_date = entry_posted_datetime
            entry_posted = entry_posted_datetime.isoformat()
            logger.debug(f"entry_posted: {entry_posted}")
            entry_id = entry_config.get('id') or generate_id(entry_posted,entry_title,entry_href,digits=12)
            logger.debug(f"entry_id: {entry_id}")
        else:
            entry_posted = None
            entry_id = entry_config.get('id') or generate_id(entry_posted,entry_title,entry_href,digits=12)
            logger.debug(f"entry_id: {entry_id}")
            entry_posted = now.isoformat()
        
        # if len(entry_config['id']) > 0:
        #     entry_id = entry_config['id']
        # else:
        #     entry_id = generate_id(entry_posted_datetime,entry_title,entry_href,digits=12)
        # entry_category = 
        entry_info_list.append({
            'id': entry_id,
            'title': entry_title.strip(),
            'link': entry_href.strip(),
            'updated': entry_posted
        })
    
        n+=1
        if n > limit_entries:
            break
    
    
    feed_config = site_config['feed']
    # feed_title = feed_config['title'] or ''.join([str(s.replace('　',' ').strip()) for s in lxml_converted_data.xpath(feed_config['title_xpath'])])
    feed_title = feed_config.get('title') or ''.join([str(s.replace('　',' ')) for s in lxml_converted_data.xpath(feed_config['title_xpath'])])
    feed_link = feed_config.get('link') or "{url_parsed.hostname}{url_parsed.path}".format(
            # url_parsed=urlparse(''.join([str(s.replace('　',' ').strip()) for s in lxml_converted_data.xpath(feed_config['link_xpath'])]))
            url_parsed=urlparse(''.join([str(s.replace('　',' ')) for s in lxml_converted_data.xpath(feed_config['link_xpath'])]))
        )
    # feed_author = feed_config['author'] or ''.join([str(s.replace('　',' ').strip()) for s in lxml_converted_data.xpath(feed_config['author_xpath'])])
    feed_author = feed_config.get('author') or ''.join([str(s.replace('　',' ')) for s in lxml_converted_data.xpath(feed_config['author_xpath'])])
    feed_id = feed_config.get('id') or f"tag:{urlparse(site_config['url']).hostname},9999:{urlparse(site_config['url']).path}"
    
    feed_info = {
        'name': site_config.get('feed_name',""),
        'title': feed_title.strip(),
        'link': feed_link.strip(),
        'author': feed_author.strip(),
        'id': feed_id,
        'updated': latest_date.isoformat() if latest_date is not None else now.isoformat(),
        'last_scraped': now.isoformat()
    }

    return feed_info,entry_info_list

