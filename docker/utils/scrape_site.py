from selenium import webdriver # type: ignore
from selenium.webdriver.common.by import By # type: ignore
from selenium.webdriver.support.ui import WebDriverWait # type: ignore
from selenium.webdriver.support import expected_conditions as EC # type: ignore
from bs4 import BeautifulSoup # type: ignore
import time
import xml.etree.ElementTree as ET
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)

def scrape_site(target_url,remote_mode=False,headless_mode=True):
    logger.info(f"func 'scrape_site' called. Options: target_url={target_url}, remote_mode={remote_mode},headless_mode={headless_mode}")
    
    options = webdriver.FirefoxOptions()
    logger.info("Selenium options making")

    if headless_mode==True:
        logger.info("Selenium headless mode")
        options.add_argument("--headless=new")
        # options.add_argument("--headless")

    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    options.add_argument("--window-size=1280x1696")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-setuid-sandbox")
    options.add_argument("--hide-scrollbars")
    options.add_argument("--enable-logging")
    options.add_argument("--log-level=0")
    options.add_argument("--single-process")

    options.add_argument("--homedir=/tmp")
    options.add_argument("--user-data-dir=/tmp/user-data")
    options.add_argument("--data-path=/tmp/data-path")
    options.add_argument("--disk-cache-dir=/tmp/cache-dir")
    options.add_argument("--profile-root=/tmp")
    options.add_argument("--media-cache-dir=/tmp/media-cache")    
    
    options.binary_location = "/opt/firefox/firefox"

    logger.info("Selenium options added")


    # Selenium Server に接続する
    if remote_mode==True:
        logger.info("Selenium remote server calling")

        driver = webdriver.Remote(
            command_executor='http://localhost:4444/wd/hub',
            options=options,
        )
    else:
        logger.info("Local mode executed")
        driver = webdriver.Firefox(options=options,)
        # driver = webdriver.Firefox(options=options)


    try:
        logger.info(f"scraping target: {target_url}")
        driver.get(target_url)
        
        # タイマーの開始
        start_time = time.time()
        max_duration = 150  # タイムアウト時間（秒）
        
        
        # スクロール回数の上限を設定
        n_scroll = 0
        max_scroll = 20

        # ページの完全な読み込みを待機
        WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.TAG_NAME, 'body'))
        )

        # スクロール操作をシミュレート
        last_height = driver.execute_script("return document.body.scrollHeight")
        while True:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(3)  # ページのロードを待つ
            new_height = driver.execute_script("return document.body.scrollHeight")

            # 経過時間をチェック
            elapsed_time = time.time() - start_time
            if elapsed_time > max_duration:
                print(f"{max_duration} seconds elapsed. Timeout reached, ending scroll operation.")
                break 

            if new_height == last_height:
                break

            n_scroll += 1
            if n_scroll > max_scroll:
                break

            last_height = new_height
        
        # ページのソースコードを取得
        logger.info("scraping finished")
        html_source = driver.page_source
        
        # BeautifulSoupで解析
        logger.info("parse starting")
        soup_bs4_parsed = BeautifulSoup(html_source, 'html.parser')
    except KeyboardInterrupt:
        print("Keyboard Interrupted. Quitting...")
    finally:
        driver.quit()
    
    logger.info("soup making finished")
    return soup_bs4_parsed

