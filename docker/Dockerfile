# Perplexityバージョン

# ビルドステージ
FROM public.ecr.aws/lambda/python:3.12 AS builder

WORKDIR /var/task

# 必要なパッケージのインストール
RUN dnf -y install \
    wget \
    which \
    xorg-x11-server-Xvfb \
    gtk3 \
    dbus-glib \
    dbus-common \
    alsa-lib \
    mesa-libGL \
    mesa-libEGL \
    libXScrnSaver \
    xdg-utils \
    tar \
    bzip2 \
    && dnf clean all

# Firefoxのインストール
RUN wget -q -O /tmp/firefox.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US" \
    && tar xjf /tmp/firefox.tar.bz2 -C /opt/ \
    && ln -s /opt/firefox/firefox /usr/bin/firefox

# GeckoDriverのインストール
RUN GECKODRIVER_VERSION=$(curl -s https://api.github.com/repos/mozilla/geckodriver/releases/latest | grep '"tag_name"' | sed -E 's/.*"v([^"]+)".*/\1/') \
    && wget -q -O /tmp/geckodriver.tar.gz "https://github.com/mozilla/geckodriver/releases/download/v${GECKODRIVER_VERSION}/geckodriver-v${GECKODRIVER_VERSION}-linux64.tar.gz" \
    # && tar -xzf /tmp/geckodriver.tar.gz -C /usr/local/bin \
    # && chmod +x /usr/local/bin/geckodriver
    && tar -xzf /tmp/geckodriver.tar.gz -C /usr/local/bin

# アプリケーションコードのコピー
COPY lambda_function.py ${LAMBDA_TASK_ROOT}
COPY ./utils ${LAMBDA_TASK_ROOT}/utils
COPY ./utils_devtool ${LAMBDA_TASK_ROOT}/utils_devtool
COPY ./utils_lambda ${LAMBDA_TASK_ROOT}/utils_lambda
COPY rss_sites_wikipedia_onthisday.json ${LAMBDA_TASK_ROOT}

# 実行ステージ
FROM public.ecr.aws/lambda/python:3.12

WORKDIR /var/task

# 必要なライブラリのインストール（最小限に抑える）
RUN dnf -y install \
    gtk3 \
    dbus-glib \
    alsa-lib \
    mesa-libGL \
    mesa-libEGL \
    libXScrnSaver \
    && dnf clean all

# Pythonパッケージのインストール
COPY requirements.txt .
RUN pip install -r requirements.txt

# ビルドステージから必要なファイルをコピー
COPY --from=builder /opt/firefox /opt/firefox
COPY --from=builder /usr/local/bin/geckodriver /usr/local/bin/geckodriver
COPY --from=builder /var/task ${LAMBDA_TASK_ROOT}

# シンボリックリンクの再作成
RUN ln -s /opt/firefox/firefox /usr/bin/firefox

# 環境変数の設定
ENV SE_CACHE_PATH="/tmp/cache"

# コマンドの設定
CMD [ "lambda_function.handler" ]
