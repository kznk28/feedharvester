import json
import sqlite3
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def save_info(feed_info,entry_info_list,db_path):
    connect = sqlite3.connect(db_path)
    # connect.row_factory = sqlite3.Row # 要らんか
    cursor = connect.cursor()
    
    feed_name = feed_info.get("name")
    feed_id = feed_info.get("id")
    if feed_id is None:
        return

    cursor.execute("create table if not exists feeds_info(feed_name varchar, feed_id varchar, params json, primary key(feed_name))")
    elems = json.dumps({k:v for k,v in feed_info.items() if k not in ["id","name"]})
    cursor.execute("replace into feeds_info(feed_name,feed_id,params) values(?,?,?)",(feed_name,feed_id,elems))

    
    cursor.execute("create table if not exists entries_info(feed_id varchar, entry_id varchar, params json, primary key(feed_id, entry_id))")
    for entry in entry_info_list:
        entry_id = entry.get("id")
        if entry_id is None:
            return
        
        elems = json.dumps({k:v for k,v in entry.items() if k != "id"})
        cursor.execute("insert or ignore into entries_info(feed_id, entry_id, params) values(?,?,?)",(feed_id,entry_id,elems))
   
    connect.commit()
    connect.close()

