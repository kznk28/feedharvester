import sqlite3

def query_db(query,db_path):
    connect = sqlite3.connect(db_path)
    connect.row_factory = sqlite3.Row
    cursor = connect.cursor()
    res = cursor.execute(query).fetchall()
    connect.close()

    return res

