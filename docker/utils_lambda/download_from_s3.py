import boto3 # type: ignore
from botocore.exceptions import NoCredentialsError, ClientError # type: ignore

def download_from_s3(bucket, object_name, file_name):
    """Download a file from an S3 bucket

    :param bucket: Bucket to download from
    :param object_name: S3 object name
    :param file_name: File to save as
    :return: True if file was downloaded, else False
    """

    # Download the file
    s3_client = boto3.client('s3')
    try:
        s3_client.download_file(bucket, object_name, file_name)
    except NoCredentialsError:
        print("Credentials not available")
        return False
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
            return False
        else:
            raise
    return True