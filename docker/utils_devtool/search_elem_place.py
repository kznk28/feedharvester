from lxml import html # type: ignore
from html import unescape
import logging

from utils.scrape_site import scrape_site


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def search_elem_place(target_url,entries_xpath=None,target_elem_xpath=None,**kwargs):
    """ 本番運用には使わない。XPath同定作業中に使う """
    # selenium/standalone-firefoxを使うとき：
    # docker run -d -p 4444:4444 -p 7900:7900 --shm-size="2g" selenium/standalone-firefox

    headless_mode = kwargs.get('headless_mode',True) # デフォルトでヘッドレスモード
    remote_mode = kwargs.get('remote_mode',False) # デフォルトでローカルモード（非リモートモード）
    
    logger.info(f'Make a beautiful soup. Options: headless_mode={headless_mode}, remote_mode={remote_mode}')
    
    soup = scrape_site(target_url,remote_mode=remote_mode,headless_mode=headless_mode)
    lxml_converted_data = html.fromstring(str(soup))

    logger.debug('Looks like a beautiful soup is in the works...')
    
    # metaタグの内容精査
    logger.debug('##### <meta> #####')
    for element in lxml_converted_data.xpath('//meta'):
        # print(l.tag,l.xpath('@name'),l.xpath('@content'))
        if not ('http-equiv' in element.attrib and element.attrib['http-equiv'] == 'Content-Security-Policy'):
            logger.debug(element.tag,element.attrib)
    logger.debug('##### </meta> #####\n')

    # 各エントリーごとの要素選定
    if not entries_xpath is None:
        try:
            entries = lxml_converted_data.xpath(entries_xpath)
            logger.debug('##### <entry> #####')
            logger.debug(f"{len(entries)} entries found")
            logger.debug(f'entry_xpath = {entries_xpath}')
            logger.debug(unescape(html.tostring(entries[0], pretty_print=True).decode()))
            logger.debug('##### </entry> #####\n')
            if not target_elem_xpath is None:
                target_elem = entries[0].xpath(target_elem_xpath)
                logger.debug('##### <target_elem> #####')
                logger.debug(f'target_elem_xpath = {target_elem_xpath}')
                print(target_elem)
                logger.debug(unescape(html.tostring(target_elem, pretty_print=True).decode()))
                logger.debug('##### </target_elem> #####')

        except Exception as e:
            logger.info(e)

    logger.info('Okay, enjoy the soup!')
    return lxml_converted_data