import os
from utils_lambda import upload_to_s3

def upload_directory_to_s3(directory, bucket, s3_base_path, exclude_patterns=None):
    """Upload a directory and its contents to an S3 bucket

    :param directory: Directory to upload
    :param bucket: Bucket to upload to
    :param s3_base_path: S3 base path to upload the directory contents to
    :param exclude_patterns: List of patterns to exclude from upload
    :return: True if all files were uploaded successfully, else False
    """
    exclude_patterns = exclude_patterns or []

    def is_excluded(path):
        for pattern in exclude_patterns:
            if pattern in path:
                return True
        return False

    for root, dirs, files in os.walk(directory):
        # ディレクトリの除外
        dirs[:] = [d for d in dirs if not is_excluded(os.path.join(root, d))]
        for file in files:
            file_path = os.path.join(root, file)
            if is_excluded(file_path):
                print(f"Skipping excluded file: {file_path}")
                continue
            s3_key = os.path.join(s3_base_path, os.path.relpath(file_path, directory))
            if not upload_to_s3(file_path, bucket, s3_key):
                print(f"Failed to upload {file_path} to S3")
                return False
    return True
